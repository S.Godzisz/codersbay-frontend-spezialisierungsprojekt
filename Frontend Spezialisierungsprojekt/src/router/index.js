import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from "@/views/LoginView.vue";
import MainApp from "../views/MainApp.vue";
import AboutView from "@/views/AboutView.vue";
import Register from "@/views/Register.vue";
import Test from "@/views/Test.vue";


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: AboutView
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView,
    },
    {
      path: '/register',
      name: 'Register',
      component: Register,
    },
    {
      path: '/mainApp',
      name: 'MainApp',
      component: MainApp,
    },
    {
      path: '/test',
      name: 'test',
      component: Test,
    }
  ],
});



export default router
