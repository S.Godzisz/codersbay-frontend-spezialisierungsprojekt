import { createApp } from 'vue';
import { createPinia } from 'pinia';
import router from './router';
import App from './App.vue';
import { vuetify } from './plugins/vuetify';

import '@mdi/font/css/materialdesignicons.css';
import 'vuetify/dist/vuetify.css';

const pinia = createPinia(); // Initialize Pinia first

const app = createApp(App);
app.use(pinia);
app.use(router);
app.use(vuetify);
app.mount('#app');
