import { defineStore } from 'pinia';
import axios from 'axios';
import {useRouter} from "vue-router";

export const useTodoStore = defineStore('todos', {
    state: () => ({
        todos: [],
    }),
    actions: {
        addTodo(todo) {
            this.todos.push(todo);
        },
        deleteTodo(index) {
            this.todos.splice(index, 1);
        },
        editTodo(index, newTodo, description) {
            this.todos[index] = newTodo;
        },

        setTodos(todos) {
            this.todos = todos;
        },
        async addTodoToBackend(newTodoText,newTodoDescription = '',newTodoStatus = 'TODO') {
            try {
                const authToken = this.getAuthToken();

                if (!authToken) {
                    console.error('Authentication token is missing.');
                    return;
                }

                if (newTodoText.trim() !== '') {
                    const response = await axios.post('https://codersbay.a-scho-wurscht.at/api/task', {
                        title: newTodoText,
                        description: newTodoDescription,
                    }, {
                        headers: {
                            Authorization: `Bearer ${authToken}`,
                        },
                    });

                    if (response.status === 201) {
                        const newTodoItem = {
                            title: response.data.title,  // Update the property name if it's different
                            taskId: response.data.taskId,
                            description: response.data.description,
                            status: response.data.status
                        };

                        this.addTodo(newTodoItem);
                    } else {
                        console.error('Failed to create task on the backend.');
                    }
                }
            } catch (error) {
                console.error('Error adding task:', error);
            }
        },
        async fetchUserTodos() {
            console.log('Fetching user todos in useTodoStore...');
            const router = useRouter();

            const config = {
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('access_token'),
                },
            };

            try {
                const response = await axios.get('https://codersbay.a-scho-wurscht.at/api/user/tasks', config);

                if (Array.isArray(response.data)) {
                    console.log('Updating todos in the store:', response.data);
                    this.todos = response.data.map(todo => ({
                        title: todo.title,
                        taskId: todo.taskId,
                        description: todo.description,
                        status: todo.status,
                    }));
                } else {
                    console.error('Invalid response format. Expected an array of todos.');
                }
            } catch (error) {
                console.error('Error fetching user todos in useTodoStore:', error);
                if (error.response && error.response.status === 401) {
                    // Redirect to login if we have a 401 Unauthorized response
                    throw error;
                }
            }
        },
        async deleteTodoOnBackend(index) {
            try {
                const authToken = this.getAuthToken();
                if (!authToken) {
                    console.error('Authentication token is missing.');
                    return;
                }

                const todoIdToDelete = this.todos[index].taskId;
                console.log('all tasks:', this.todos)
                console.log('index:', index)
                console.log('found:', this.todos[index].taskId)

                const response = await axios.delete(`https://codersbay.a-scho-wurscht.at/api/task/${todoIdToDelete}`, {
                    headers: {
                        Authorization: `Bearer ${authToken}`,
                        'Content-Type': 'application/json',
                    },
                });

                if (response.status === 204) {

                    this.deleteTodo(index);
                } else {
                    console.error('Failed to delete todo on the backend:', response.status, response.data);
                }
            } catch (error) {
                console.error('Error deleting todo:', error.response?.status, error.response?.data);
            }
        },

        async editTodoOnBackend(index, updatedTodoText, editedDescription ) {
            try {
                const authToken = this.getAuthToken();

                if (!authToken) {
                    console.error('Authentication token is missing.');
                    return;
                }

                const todoIdToUpdate = this.todos[index].taskId;

                const response = await axios.put(`https://codersbay.a-scho-wurscht.at/api/task/${todoIdToUpdate}`, {
                    title: updatedTodoText,
                    description : editedDescription
                }, {
                    headers: {
                        Authorization: `Bearer ${authToken}`,
                    },
                });

                if (response.status === 200) {
                    const updatedTodo = {
                        title: response.data.title,
                        taskId: response.data.taskId,
                        description: response.data.description,
                        status: response.data.status
                    };

                    this.editTodo(index, updatedTodo);
                } else {
                    console.error('Failed to edit todo on the backend.');
                }
            } catch (error) {
                console.error('Error editing todo:', error);
            }
        },
        async updateTodoStatusOnBackend(taskId, newStatus) {
            try {
                const authToken = this.getAuthToken();
                if (!authToken) {
                    console.error('Authentication token is missing.');
                    return;
                }

                const config = {
                    headers: {
                        'Authorization': `Bearer ${authToken}`,
                        'Content-Type': 'application/json',
                    },
                };

                const response = await axios.put(`https://codersbay.a-scho-wurscht.at/api/task/${taskId}`, {
                    status: newStatus,
                }, config);

                if (response.status === 200) {
                    const updatedIndex = this.todos.findIndex(todo => todo.taskId === taskId);
                    if (updatedIndex !== -1) {
                        this.todos[updatedIndex].status = newStatus;
                    } else {
                        console.error('Updated todo not found in the local state.');
                    }
                } else {
                    console.error('Failed to update todo status on the backend:', response.status);
                }
            } catch (error) {
                console.error('Error updating todo status:', error);
            }
        },


        // Add getAuthToken method
        getAuthToken() {
            return localStorage.getItem('access_token');
        },
    },
});
