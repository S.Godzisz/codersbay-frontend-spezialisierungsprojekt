import {defineStore} from 'pinia';
import axios from 'axios';

export const useAuthenticationStore = defineStore('authentication', {
    state: () => ({
        user: null,
        defaultTaskListId: null,
        email: '',
        password: '',
        firstName: '',
        lastName: '',
        username: '',
        termsOfUseAccepted: true

    }),
    actions: {
        async login({email, password}) {
            const response = await axios.post('https://codersbay.a-scho-wurscht.at/api/auth/login', {
                email: email,
                password: password
            })
            localStorage.setItem('access_token', response.data.accessToken)
            localStorage.setItem('defaultTaskListId', response.data.user.taskLists[0].taskListId)
            this.defaultTaskListId = response.data.user.taskLists[0].taskListId
            this.user = response.data.user;
        },
        async auth()        //auth-Aktion wird aufgerufen, wenn die default-Tasklist "null" ist, also beim Aktualisieren der Seite
        {
            const config =
                {
                    headers:
                        {
                            Authorization: 'Bearer ' + localStorage.getItem('access_token')
                        }
                }
            const response = await axios.get('https://codersbay.a-scho-wurscht.at/api/auth', config)
            localStorage.setItem('access_token', response.data.accessToken)
            localStorage.setItem('defaultTaskListId', response.data.user.taskLists[0].taskListId)           //hier wird die default-Taskliste des angemeldeten Users,
            this.defaultTaskListId = response.data.user.taskLists[0].taskListId                             //die in der API "api/auth" im Array "taskLists" auf dem Index 0 ist, in das Property "defaultTaskListId" gespeichert
            this.user = response.data.user;
        },
        async getUser() {

            const config =
                {
                    headers:
                        {
                            Authorization: 'Bearer ' + localStorage.getItem('access_token')
                        }
                }
            const response = await axios.get('https://codersbay.a-scho-wurscht.at/api/auth', config)
            this.user = response.data.user;


        },

        async register({email,password,firstName,lastName, username, termsOfUseAccepted}){
            const response = await axios.post('https://codersbay.a-scho-wurscht.at/api/auth/register',
                {
                email: email,
                password: password,
                firstName: firstName,
                lastName: lastName,
                username: username,
                termsOfUseAccepted: termsOfUseAccepted
            });

            localStorage.setItem('access_token', response.data.accessToken);
            localStorage.setItem('defaultTaskListId', response.data.user.taskLists[0].taskListId);
            this.defaultTaskListId = response.data.user.taskLists[0].taskListId;
            this.user = response.data.user;
        }
    },
});
